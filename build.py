import jinja2

env = jinja2.Environment(loader=jinja2.FileSystemLoader('templates'), autoescape=(['html']))

template = env.get_template('index.html')
with open('dist/index.html', 'w') as f:
    f.write(template.render(a="qwe"))

with open('js/sheet_workers.js') as f:
    sheet_workers = f.read()

template = env.get_template('main.html')
with open('dist/delta_green_by_sowa.html', 'w') as f:
    f.write(template.render(a="qwe"))
    f.write('<script type="text/worker">')
    f.write(sheet_workers)
    f.write('</script>')

with open('css/main.css') as f:
    main_css = f.read()

with open('css/normalize.css') as f:
    normalize_css = f.read()

with open('dist/main.css', 'w') as f:
    f.write(main_css)
    f.write(normalize_css)
